<?php

session_start();

require 'headers.php';

if (!empty($_FILES['blob']) && !empty($_POST['parcours'])) {
	$parcours = $_POST['parcours'];
	if (!file_exists('../fichiers/' . $parcours)) {
		mkdir('../fichiers/' . $parcours, 0775, true);
	}
	$extension = 'mp3';
	$nom = hash('md5', 'enregistrement' . uniqid('', false)) . time() . '.' . $extension;
	$chemin = '../fichiers/' . $parcours . '/' . $nom;
	if (move_uploaded_file($_FILES['blob']['tmp_name'], $chemin)) {
		echo $nom;
	} else {
		echo 'erreur';
	}
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
