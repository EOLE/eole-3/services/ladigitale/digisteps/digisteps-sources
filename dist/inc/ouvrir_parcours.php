<?php

session_start();

require 'headers.php';

if (!empty($_POST['parcours']) && !empty($_POST['question']) && !empty($_POST['reponse'])) {
	require 'db.php';
	$reponse = '';
	$parcours = $_POST['parcours'];
	if (isset($_SESSION['digisteps'][$parcours]['reponse'])) {
		$reponse = $_SESSION['digisteps'][$parcours]['reponse'];
	}
	$question = $_POST['question'];
	$reponse = strtolower($_POST['reponse']);
	$stmt = $db->prepare('SELECT question, reponse FROM digisteps_parcours WHERE url = :url');
	if ($stmt->execute(array('url' => $parcours))) {
		$resultat = $stmt->fetchAll();
		$questionSecrete = '';
		if (!$resultat) {
			echo 'contenu_inexistant';
		} else {
			switch ($resultat[0]['question']) {
				case 'Quel est mon mot préféré ?':
					$questionSecrete = 'motPrefere';
					break;
				case 'Quel est mon film préféré ?':
					$questionSecrete = 'filmPrefere';
					break;
				case 'Quelle est ma chanson préférée ?':
					$questionSecrete = 'chansonPreferee';
					break;
				case 'Quel est le prénom de ma mère ?':
					$questionSecrete = 'prenomMere';
					break;
				case 'Quel est le prénom de mon père ?':
					$questionSecrete = 'prenomPere';
					break;
				case 'Quel est le nom de ma rue ?':
					$questionSecrete = 'nomRue';
					break;
				case 'Quel est le nom de mon employeur ?':
					$questionSecrete = 'nomEmployeur';
					break;
				case 'Quel est le nom de mon animal de compagnie ?':
					$questionSecrete = 'nomAnimal';
					break;
				default:
					$questionSecrete = $resultat[0]['question'];
			}
			$reponseSecrete = $resultat[0]['reponse'];
			if ($question === $questionSecrete && password_verify($reponse, $reponseSecrete)) {
				$_SESSION['digisteps'][$parcours]['reponse'] = $reponseSecrete;
				$type = $_POST['type'];
				if ($type === 'api' && !isset($_SESSION['digisteps'][$parcours]['digidrive'])) {
					$_SESSION['digisteps'][$parcours]['digidrive'] = 1;
				}
				echo 'parcours_debloque';
			} else {
				echo 'non_autorise';
			}
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
