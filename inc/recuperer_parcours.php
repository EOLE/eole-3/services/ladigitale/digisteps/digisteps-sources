<?php

session_start();

require 'headers.php';

if (!empty($_POST['id'])) {
	require 'db.php';
	$reponse = '';
	$id = $_POST['id'];
	if (isset($_SESSION['digisteps'][$id]['reponse'])) {
		$reponse = $_SESSION['digisteps'][$id]['reponse'];
	}
	$stmt = $db->prepare('SELECT * FROM digisteps_parcours WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		if ($parcours = $stmt->fetchAll()) {
			$admin = false;
			if (count($parcours, COUNT_NORMAL) > 0 && $parcours[0]['reponse'] === $reponse) {
				$admin = true;
			}
			$donnees = $parcours[0]['donnees'];
			if ($donnees !== '') {
				$donnees = json_decode($donnees);
			}
			$digidrive = 0;
			if (isset($_SESSION['digisteps'][$id]['digidrive'])) {
				$digidrive = $_SESSION['digisteps'][$id]['digidrive'];
			}
			echo json_encode(array('nom' => $parcours[0]['nom'], 'donnees' => $donnees, 'admin' =>  $admin, 'digidrive' => $digidrive));
		} else {
			echo 'contenu_inexistant';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
