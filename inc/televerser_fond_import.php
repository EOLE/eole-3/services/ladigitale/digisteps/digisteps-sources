<?php

session_start();

require 'headers.php';

if (!empty($_FILES['blob']) && !empty($_POST['parcours'])) {
	$fichier = $_POST['fichier'];
	$parcours = $_POST['parcours'];
	$extension = pathinfo($fichier, PATHINFO_EXTENSION);
	if (!file_exists('../fichiers/' . $parcours)) {
		mkdir('../fichiers/' . $parcours, 0775, true);
	}
	$chemin = '../fichiers/' . $parcours . '/' . $fichier;
	if (move_uploaded_file($_FILES['blob']['tmp_name'], $chemin)) {
		echo 'fichier_importe';
	} else {
		echo 'erreur';
	}
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
